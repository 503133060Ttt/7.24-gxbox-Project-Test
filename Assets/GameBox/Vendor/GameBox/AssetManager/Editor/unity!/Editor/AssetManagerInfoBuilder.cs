using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using AssetBundleGraph;
using GameBox.AssetManager;
using UnityEditor;
using Model = UnityEngine.AssetGraph.DataModel.Version2;

namespace UnityEngine.AssetGraph
{
    [CustomNode("Build AssetManagerInfo", 100)]
    public class AssetManagerInfoBuider : Node, Model.NodeDataImporter
    {
        [SerializeField] private SerializableMultiTargetString m_outputDir;
        [SerializeField] private SerializableMultiTargetString m_Version = new SerializableMultiTargetString("0.0.0.1");
        [SerializeField] private SerializableMultiTargetInt m_binarize = new SerializableMultiTargetInt(0);
        [SerializeField] private SerializableMultiTargetInt overrideTarget = new SerializableMultiTargetInt(0);
        public override string Category
        {
            get
            {
                return "BuildAssetManagerInfo";
            }
        }

        public override string ActiveStyle
        {
            get
            {
                return "node 8 on";
            }
        }

        public override string InactiveStyle
        {
            get
            {
                return "node 8";
            }
        }

        public override Model.NodeOutputSemantics NodeInputType
        {
            get
            {
                return Model.NodeOutputSemantics.AssetBundles;
            }
        }

        public override Model.NodeOutputSemantics NodeOutputType
        {
            get
            {
                return Model.NodeOutputSemantics.None;
            }
        }

        public override Node Clone(Model.NodeData newData)
        {
            var newNode = new AssetManagerInfoBuider();
            newNode.m_outputDir = new SerializableMultiTargetString(m_outputDir);
            newNode.m_Version = new SerializableMultiTargetString(m_Version);
            newData.AddDefaultInputPoint();
            newData.AddDefaultOutputPoint();

            return newNode;
        }

        public void Import(NodeData v1, Model.NodeData v2)
        {
            m_outputDir = new SerializableMultiTargetString();
            m_Version = new SerializableMultiTargetString("0.0.0.1");
        }

        public override void Initialize(Model.NodeData data)
        {
            m_outputDir = new SerializableMultiTargetString();
            m_Version = new SerializableMultiTargetString("0.0.0.1");
            data.AddDefaultInputPoint();
            data.AddDefaultOutputPoint();
        }

        public override void OnInspectorGUI(NodeGUI node, AssetReferenceStreamManager streamManager, NodeGUIEditor editor, Action onValueChanged)
        {
            EditorGUILayout.HelpBox("Build AssetManagerInfo: Build assetManager info with given version settings.", MessageType.Info);
            editor.UpdateNodeName(node);
            EditorGUILayout.Space();

            editor.DrawPlatformSelector(node);
            using (new EditorGUILayout.VerticalScope(GUI.skin.box))
            {
                var disabledScope = editor.DrawOverrideTargetToggle(node, overrideTarget[editor.CurrentEditingGroup] == 1, (bool enabled) =>
                {
                    using (new RecordUndoScope("Remove Target Bundle Options", node, true))
                    {
                        if (enabled)
                        {
                            m_Version[editor.CurrentEditingGroup] = m_Version.DefaultValue;
                            m_outputDir[editor.CurrentEditingGroup] = m_outputDir.DefaultValue;
                        }
                        else
                        {
                            m_Version.Remove(editor.CurrentEditingGroup);
                            m_outputDir.Remove(editor.CurrentEditingGroup);
                        }
                        
                        overrideTarget[editor.CurrentEditingGroup] = enabled ? 1 : 0;
                        onValueChanged();
                    }
                });
                m_Version[editor.CurrentEditingGroup] = EditorGUILayout.TextField("Version", m_Version[editor.CurrentEditingGroup]);

                EditorGUILayout.Space();
                m_binarize[editor.CurrentEditingGroup] = EditorGUILayout.ToggleLeft("AssetManagerInfo binarize build", m_binarize[editor.CurrentEditingGroup] == 1) ? 1 : 0;

                EditorGUILayout.Space();
                string newputputDir = editor.DrawFolderSelector("Output Directory", "Select Output Folder",
                    m_outputDir[editor.CurrentEditingGroup],
                    Application.dataPath,
                    (string folderSelected) =>
                    {
                        return folderSelected;
                    }
                );
                
                if (newputputDir != m_outputDir[editor.CurrentEditingGroup])
                {
                    using (new RecordUndoScope("Change Output Directory", node, true))
                    {
                        m_outputDir[editor.CurrentEditingGroup] = newputputDir;
                        onValueChanged();
                    }
                }
                
                if (Directory.Exists(m_outputDir[editor.CurrentEditingGroup]))
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        GUILayout.FlexibleSpace();
#if UNITY_EDITOR_OSX
                        string buttonName = "Reveal in Finder";
#else
                        string buttonName = "Show in Explorer";
#endif
                        if (GUILayout.Button(buttonName))
                        {
                            EditorUtility.RevealInFinder(m_outputDir[editor.CurrentEditingGroup]);
                        }
                    }
                }
                else
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        if (GUILayout.Button("Create Finder"))
                        {
                            using (new RecordUndoScope("Create Finder", node, true))
                            {
                                Directory.CreateDirectory(m_outputDir[editor.CurrentEditingGroup]);
                                onValueChanged();
                            }
                        }
                    }
                }
            }
        }

        public override void Prepare(BuildTarget target,
            Model.NodeData node,
            IEnumerable<PerformGraph.AssetGroups> incoming,
            IEnumerable<Model.ConnectionData> connectionsToOutput,
            PerformGraph.Output Output)
        {
            
            // BundleBuilder do nothing without incoming connections
            if (incoming == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(m_outputDir[target]))
            {
                throw new NodeException("Output directory is empty.",
                    "Select valid output directory from inspector.", node);
            }
        }

        public override void Build(BuildTarget target,
            Model.NodeData node,
            IEnumerable<PerformGraph.AssetGroups> incoming,
            IEnumerable<Model.ConnectionData> connectionsToOutput,
            PerformGraph.Output Output,
            Action<Model.NodeData, string, float> progressFunc)
        {
            if (incoming == null)
            {
                return;
            }

            AssetBundle.UnloadAllAssetBundles(true);

            string outputDir = m_outputDir[target] + "/AssetManager.bundle";
            FileUtility.RemakeDirectory(outputDir);

            AssetDatabase.Refresh();
            
            if (progressFunc != null) 
            {
                progressFunc(node, "Building AssetManagerInfo ...", 0.0f);
            }

            var manifestDirs = new Dictionary<string, AssetBundleManifest>();
            var bundleDirs = new Dictionary<string, AssetBundle>();
            foreach (var ag in incoming)
            {
                foreach (var item in ag.assetGroups)
                {
                    foreach (var asset in item.Value)
                    {
                        if (asset.importFrom.Contains(".manifest") || asset.isSceneAsset)
                        {
                            continue;
                        }
                        
                        string path = Application.dataPath + asset.importFrom.Replace("Assets", ""); 
                        if (!File.Exists(path))
                        {
                            path = asset.importFrom;
                        }
                        
                        var manifestBundle = AssetBundle.LoadFromFile(path);
                        
                        if (!bundleDirs.ContainsKey(path))
                        {
                            bundleDirs.Add(path, manifestBundle);
                        }
                        else
                        {
                            bundleDirs[path] = manifestBundle;
                        }
                        
                        if (manifestBundle == null || manifestBundle.isStreamedSceneAssetBundle)
                        {
                            continue;
                        }
                        
                        var manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                        if (manifest == null)
                        {
                            continue;
                        }

                        string dirPath = Path.GetDirectoryName(path);

                        if (!manifestDirs.ContainsKey(dirPath))
                        {
                            manifestDirs.Add(dirPath, manifest);
                        }
                        else
                        {
                            manifestDirs[dirPath] = manifest;
                        }
                        
                        break;
                    }
                }
            }
            
            if (progressFunc != null) 
            {
                progressFunc(node, "Building AssetManagerInfo ...", 0.5f);
            }
            
            var assetPackInfos = new Dictionary<string, AssetPackInfo>();
            var assetInfos = new Dictionary<string, AssetInfo>();
            foreach (var item in manifestDirs)
            {
                AssetBundleManifest manifest = item.Value;
                foreach (var abName in manifest.GetAllAssetBundles())
                {
                    string abFilePath = item.Key + "/" +abName;
                    AssetBundle ab;
                    if (bundleDirs.ContainsKey(abFilePath))
                    {
                        ab = bundleDirs[abFilePath];
                    }
                    else
                    {
                        ab = AssetBundle.LoadFromFile(abFilePath);
                    }
                    
                    var abFile = new FileInfo(abFilePath);
                    if (abFile.Exists && ab!=null)
                    {
                        string newAbFilePath = outputDir + abFilePath.Replace(item.Key, "");
                        string newAbFileDirPath = Path.GetDirectoryName(newAbFilePath);
                        if (!Directory.Exists(newAbFileDirPath))
                        {
                            Directory.CreateDirectory(newAbFileDirPath);
                        }

                        abFile.CopyTo(newAbFilePath);

                        var dependencies = manifest.GetAllDependencies(abName);
                        assetPackInfos.Add(abName, new AssetPackInfo
                        {
                            Type = AssetPackType.Bundle,
                            Dependencies = (dependencies.Length > 0 ? dependencies : null),
                            CheckCode = ComputeMD5CodeForFile(abFilePath),
                            Size = abFile.Length,
                        });

                        if (ab.isStreamedSceneAssetBundle)
                        {
                            var scenes = ab.GetAllScenePaths();
                            for (int i = 0; i < scenes.Length; i++)
                            {
                                var info = new AssetInfo()
                                {
                                    PackPath = abName,
                                };
                                assetInfos.Add(scenes[i].Replace("Assets/", ""), info);
                            }
                        }

                        var assets = ab.GetAllAssetNames();
                        for (var j = 0; j < assets.Length; ++j)
                        {
                            var info = new AssetInfo()
                            {
                                PackPath = abName,
                            };
                            assetInfos.Add(assets[j].Replace("assets/", ""), info);
                        }
                    }
                    else
                    {
                        Debug.Log("abFile not Exists path : " + abFilePath);
                    }
                }
            }
            
            if (progressFunc != null) 
            {
                progressFunc(node, "Building AssetManagerInfo ...", 0.7f);
            }
            
            var outputFile = outputDir + "/assetmanagerinfo.bytes";
            var assetManagerInfo = new AssetManagerInfo
            {
                Version = m_Version[target],
                Date = DateTime.UtcNow,
                Packs = assetPackInfos,
                Assets = assetInfos,
            };
            
            using (var stream = new FileStream(outputFile, FileMode.CreateNew))
            {
                AssetManagerInfo.Serialize(stream, assetManagerInfo, m_binarize[target] == 1);
                stream.Close();
            }
            
            Directory.CreateDirectory("Assets/AssetManagerTemp");
            string tempInfoFilePath = "Assets/AssetManagerTemp/assetmanagerinfo.bytes";
            File.Copy(outputFile, tempInfoFilePath,true);
            
            AssetDatabase.Refresh();
            if (progressFunc != null)
            {
                progressFunc(node, "Building AssetManagerInfo ...", 0.8f);   
            }

            var build = new AssetBundleBuild();
            build.assetBundleName = "assetmanagerinfo.bytes.ab";
            build.assetNames = new string[] { tempInfoFilePath };
            var builds = new List<AssetBundleBuild>();
            builds.Add(build);
            BuildPipeline.BuildAssetBundles(outputDir, builds.ToArray(), BuildAssetBundleOptions.UncompressedAssetBundle, target);
            
            Directory.Delete("Assets/AssetManagerTemp",true);
            
            File.Delete(outputDir + "/" + Path.GetFileName(outputDir));
            File.Delete(outputDir + "/" + Path.GetFileName(outputDir) + ".manifest");
            
            AssetDatabase.Refresh();
            if (progressFunc != null) 
            {
                progressFunc(node, "Building AssetManagerInfo ...", 0.9f);
            }
            
            AssetBundle.UnloadAllAssetBundles(true);
            
            if (progressFunc != null) 
            {
                progressFunc(node, "Building AssetManagerInfo ...", 1);
            }
        }

        private string ComputeMD5CodeForFile(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var md5 = new MD5CryptoServiceProvider();
                var result = md5.ComputeHash(stream);
                var builder = new StringBuilder();
                for (var i = 0; i < result.Length; ++i)
                {
                    builder.Append(result[i].ToString("x2"));
                }
                
                return builder.ToString();
            }
        }
    }
}
