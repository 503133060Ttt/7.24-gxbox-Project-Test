﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBox;
using GameBox.Logger;
using GameBox.MonoDriver;
using GameBox.AssetManager;

public class a123 : MonoBehaviour,IBootstrap
{
    public void Bootstrap()
    {
        GBox.Register(new MonoDriverProvider());
        GBox.Register(new UnityLoggerProvider());
        GBox.Register(new AssetManagerProvider());
    }
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        GBoxFramework app = new GBoxFramework();
        app.Instance<Component>(this);
        app.Bootstrap(this);
        app.Init();
        app.DebugLevel = DebugLevels.Development;

    }
    // Use this for initialization
    void Start () {
        var logger = GBox.Make<GameBox.Logger.ILogger>();
        //var logger = GBox.Make<ILogger>();
        logger.Info("gamestart");
       // var asset = GBox.Make<IAssetManager>().Load<GameObject>("Resources/Sphere");
       // GameObject.Instantiate(asset.Original,new Vector3(0,0,0),new Quaternion(0,0,0,0));
    }
	
	// Update is called once per frame
	void Update () {
        
    }
}
